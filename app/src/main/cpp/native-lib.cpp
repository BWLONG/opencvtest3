#include <jni.h>
#include <string>

#include <opencv2/core/core.hpp>
#include <opencv2/objdetect/objdetect.hpp>
#include <opencv2/opencv.hpp>

//static cv::CascadeClassifier* face_detecter = nullptr;
//
//// 初始化分类器
//extern "C" JNIEXPORT jlong JNICALL//void
//Java_com_chengyuan_opencvtest3_MainActivity_FaceDetecterInit(
//        JNIEnv  *jenv,
//        jobject /* this */,
//        jstring cascadeFileName
//){
//    const char* cascade_file_name = jenv->GetStringUTFChars(cascadeFileName, NULL);
//    if( face_detecter == nullptr){
//        face_detecter = new cv::CascadeClassifier(cascade_file_name);
//    }
//}
//extern "C"
//JNIEXPORT jlong JNICALL
//Java_com_chengyuan_opencvtest3_MainActivity_FaceDetecterInit(JNIEnv *env, jobject instance,
//                                                             jstring cascadeFileName_) {
//    const char *cascadeFileName = env->GetStringUTFChars(cascadeFileName_, 0);
//
//    // TODO
//
//    env->ReleaseStringUTFChars(cascadeFileName_, cascadeFileName);
//}
// 找出人脸所在位置并标记出来
//extern "C" JNIEXPORT void JNICALL
//Java_com_chengyuan_opencvtest3__MainActivity_DetectFaces(
//        JNIEnv  /* *env */,
//        jobject /* this */,
//        jlong addrInputRgbaImage
//)
//{
//    cv::Mat& image_input = *(cv::Mat*)addrInputRgbaImage;
//
//    cv::Mat image_gray;
//    cv::cvtColor(image_input, image_gray, cv::COLOR_RGBA2GRAY);
//
//    auto width = image_input.size().width;
//    auto height = image_input.size().height;
//    if(face_detecter != nullptr){
//        std::vector<cv::Rect> faces;
//        face_detecter->detectMultiScale( image_gray, faces, 1.1, 2, 0|cv::CASCADE_SCALE_IMAGE, cv::Size(width/10, height/5));
//
//        for(auto face_rect: faces){
//            cv::rectangle(image_input, face_rect, cv::Scalar(255, 0, 0), 3);
//        }
//    }
//}
//extern "C"
//JNIEXPORT jlong JNICALL
//Java_com_chengyuan_opencvtest3_MainActivity_DetectFaces(JNIEnv *env, jobject instance,
//                                                        jlong addrInputRgbaImage) {
//    cv::Mat& image_input = *(cv::Mat*)addrInputRgbaImage;
//
//    cv::Mat image_gray;
//    cv::cvtColor(image_input, image_gray, cv::COLOR_RGBA2GRAY);
//
//    auto width = image_input.size().width;
//    auto height = image_input.size().height;
//    if(face_detecter != nullptr){
//        std::vector<cv::Rect> faces;
//        face_detecter->detectMultiScale( image_gray, faces, 1.1, 2, 0|cv::CASCADE_SCALE_IMAGE, cv::Size(width/10, height/5));
//
//        for(auto face_rect: faces){
//            cv::rectangle(image_input, face_rect, cv::Scalar(255, 0, 0), 3);
//        }
//    }
//    // TODO
//
//}
extern "C"
JNIEXPORT jbyteArray JNICALL
Java_com_chengyuan_opencvtest3_MainActivity_doSomethingNeeded(JNIEnv *env, jobject instance,
                                                              jbyteArray data_) {
//    jbyte* bytedata = (*env)->GetByteArrayElements(env,data, 0);

//    jsize  oldsize = (*env)->GetArrayLength(env,data);

//    unsigned char* old = (unsigned char*)bytedata;

//    unsigned char* bytearr=myobfuscate(old);

//    jbyteArray jarray = (*env)->NewByteArray(env,oldsize);

//    (*env)->SetByteArrayRegion(env,jarray, 0, oldsize,bytearr);
//    jbyte *data = env->GetByteArrayElements(data_, NULL);
//    // TODO
//    env->ReleaseByteArrayElements(data_, data, 0);

//1. 获取数组指针和长度
    jbyte *c_array = env->GetByteArrayElements(data_, 0);
    int len_arr = env->GetArrayLength(data_);

    //2. 具体处理
    jbyteArray c_result = env->NewByteArray(len_arr);
    jbyte buf[len_arr];
    for(int i=0; i<len_arr; i++){
        buf[i] = c_array[i] + 1;
    }

    //3. 释放内存
    env->ReleaseByteArrayElements(data_, c_array, 0);

    //4. 赋值
    env->SetByteArrayRegion(c_result, 0, len_arr, buf);
    return c_result;
//    //创建一个指定大小的数组
//
//    jintArray jint_arr = env->NewIntArray(env, 10);
//
//    jint *elems = env->GetIntArrayElements(env, jint_arr, NULL);
//
//    int i = 0;
//
//    for (; i < 10; i++){
//
//        elems[i] = i;
//
//    }
//    env->ReleaseIntArrayElements(env, jint_arr, elems, 0);
//    return jint_arr;
//    return *data;
    //jintArray -> jint指针 -> c int 数组

//    jint *elems = (*env)->GetIntArrayElements(env, arr, NULL);

    //printf("%#x,%#x\n", &elems, &arr);

    //数组的长度

//    int len = (*env)->GetArrayLength(env, arr);

    //排序

//    qsort(elems, len, sizeof(jint), compare);

    //同步

    //mode

    //0, Java数组进行更新，并且释放C/C++数组

    //JNI_ABORT, Java数组不进行更新，但是释放C/C++数组

    //JNI_COMMIT，Java数组进行更新，不释放C/C++数组（函数执行完，数组还是会释放）

//    (*env)->ReleaseIntArrayElements(env, arr, elems, JNI_COMMIT);

}
extern "C"
//静态注册Native方法：
//仔细看这个函数会发现方法名的组成为 Java_包名类名方法名。
// 其中JNIEXPORT和JNICALL是两个宏定义，
// 主要作用就是说明该函数为JNI函数，
// jstring是定义JNI定义的数据类型。
// 在Java虚拟机加载的时候回连接对应的Native方法，
// 比如在例子中的 public native String getId(); 通过javah操作后会生成

JNIEXPORT jstring JNICALL
Java_com_chengyuan_opencvtest3_MainActivity_stringFromJNI
//JNIEnv结构体
//JNIEnv结构体指向一个函数表，该函数表指向一系列的JNI函数，
// 我们通过调用这些JNI函数可以与Java层进行交互，以下是常用的函数：
//..........
//jfieldID GetFieldID(jclass clazz, const char* name, const char* sig)
//jboolean GetBooleanField(jobject obj, jfieldID fieldID)
//jmethodID GetMethodID(jclass clazz, const char* name, const char* sig)
//CallVoidMethod(jobject obj, jmethodID methodID, ...)
//CallBooleanMethod(jobject obj, jmethodID methodID, ...)
//..........
//GetFieldID函数是获取Java对象中某个变量的ID
//GetBooleanField()函数是根据变量的ID获取数据类型为Boolean
//GetMethodID函数获取Java对象中对应的方法ID
//CallVoidMethod根据methodID调用对应对象中的方法，并且该方法的返回值Void类型
//CallBooleanMethod根据methodID调用对应对象中的方法，并且该方法的返回值Boolean类型

(JNIEnv *env, jobject instance) {

    // TODO


    return env->NewStringUTF("Hello");
}
extern "C"
JNIEXPORT void JNICALL
Java_com_chengyuan_opencvtest3_MainActivity_testFromJNI(JNIEnv *env, jobject instance) {
//获取jclass
    jclass j_class = env->GetObjectClass(instance);
    //获取jfieldID
    jfieldID j_fid = env->GetFieldID(j_class, "testInt", "I");
    //获取java成员变量int值
    jint j_int = env->GetIntField(instance, j_fid);
//    LOGI("noStaticField==%d", j_int);//noStaticField==0

    //Set<Type>Field    修改noStaticKeyValue的值改为666
    env->SetIntField(instance, j_fid, 666);

    //获取jclass
//    jclass j_class = env->GetObjectClass(instance);
    //获取jfieldID
//    jfieldID j_fidByteArray = env->GetFieldID(j_class, "testByteArray", "[B");
        //获取java成员变量ByteArray值
//    jbyteArray j_byteArray = env->GetObjectField(instance, j_fidByte);
//    LOGI("noStaticField==%d", j_int);//noStaticField==0

    //Set<Type>Field    修改noStaticKeyValue的值改为666
//    env->SetIntField(instance, j_fid, 666);
//    jbyte *data = env->GetIntField(instance, j_fid);
}